package com.ciandt.planocapacitacao.hibernate.examples;

import com.ciandt.planocapacitacao.hibernate.utils.JpaUtil;

public class CreateTables {

	public static void main(String[] args) {
		JpaUtil.getEntityManager();
		System.out.println("Tabelas criadas com sucesso.");
		JpaUtil.close();
	}

}
