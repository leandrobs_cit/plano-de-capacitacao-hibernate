package com.ciandt.planocapacitacao.hibernate.examples;

import javax.persistence.EntityManager;

import com.ciandt.planocapacitacao.hibernate.entity.Veiculo;
import com.ciandt.planocapacitacao.hibernate.utils.JpaUtil;

public class BuscandoVeiculo {

	public static void main(String[] args) {
		EntityManager manager = JpaUtil.getEntityManager();
		Veiculo veiculo = manager.find(Veiculo.class, 1L);

		System.out.println("O veículo de código " + veiculo.getCodigo()
				+ " é um " + veiculo.getModelo());

		manager.close();
		JpaUtil.close();

	}

}
