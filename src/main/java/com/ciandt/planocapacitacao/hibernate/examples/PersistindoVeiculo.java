package com.ciandt.planocapacitacao.hibernate.examples;

import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.ciandt.planocapacitacao.hibernate.entity.Veiculo;
import com.ciandt.planocapacitacao.hibernate.utils.JpaUtil;

public class PersistindoVeiculo {

	public static void main(String[] args) {
		EntityManager manager = JpaUtil.getEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		transaction.begin();

		Veiculo veiculo = new Veiculo();
		veiculo.setFabricante("Ford");
		veiculo.setModelo("Mustang");
		veiculo.setAnoModelo(2010);
		veiculo.setAnoFabricacao(2009);
		veiculo.setValor(new BigDecimal("200000"));

		manager.persist(veiculo);
		transaction.commit();

		manager.close();
		JpaUtil.close();
	}

}
